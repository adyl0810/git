package lesson.lesson45;

import com.sun.net.httpserver.HttpExchange;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lesson.server.BasicServer;
import lesson.server.ContentType;
import lesson.server.ResponseCodes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class LessonServer extends BasicServer {
    private final static Configuration freemarker = initFreeMarker();

    public LessonServer(String host, int port) throws IOException {
        super(host, port);
        registerGet("/employees", this::employeesHandler);
        registerGet("/employee", this::employeeHandler);
        registerGet("/books", this::booksHandler);
        registerGet("/book", this::bookHandler);
    }

    private static Configuration initFreeMarker() {
        try {
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
            cfg.setDirectoryForTemplateLoading(new File("data"));
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            cfg.setLogTemplateExceptions(false);
            cfg.setWrapUncheckedExceptions(true);
            cfg.setFallbackOnNullLoopVariable(false);
            return cfg;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    protected void renderTemplate(HttpExchange exchange, String templateFile, Object dataModel) {
        try {
            Template temp = freemarker.getTemplate(templateFile);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try (OutputStreamWriter writer = new OutputStreamWriter(stream)) {
                temp.process(dataModel, writer);
                writer.flush();
                var data = stream.toByteArray();
                sendByteData(exchange, ResponseCodes.OK, ContentType.TEXT_HTML, data);
            }
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    private void employeesHandler(HttpExchange exchange) {
        renderTemplate(exchange, "employees.html", new Employees());
    }

    private void employeeHandler(HttpExchange exchange) {
        renderTemplate(exchange, "employee.html", new Employees());
    }

    private void booksHandler(HttpExchange exchange) {
        renderTemplate(exchange, "books.html", new Books());
    }

    private void bookHandler(HttpExchange exchange) {
        renderTemplate(exchange, "book.html", new Books());
    }


}
