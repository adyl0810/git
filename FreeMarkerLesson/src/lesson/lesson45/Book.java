package lesson.lesson45;

public class Book {
    private int id;
    private String name;
    private String author;
    private String category;
    private boolean taken;

    public Book(int id, String name, String author, String category, boolean taken) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.category = category;
        this.taken = taken;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getCategory() {
        return category;
    }

    public boolean isTaken() {
        return taken;
    }
}
