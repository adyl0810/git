package lesson.lesson45;

import java.util.List;

public class Employee {
    private int id;
    private String name;
    private String surname;
    private List<Book> currentBooksTaken;
    private List<Book> allBooksTaken;

    public Employee(int id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Book> getCurrentBooksTaken() {
        return currentBooksTaken;
    }

    public List<Book> getAllBooksTaken() {
        return allBooksTaken;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setCurrentBooksTaken(List<Book> currentBooksTaken) {
        this.currentBooksTaken = currentBooksTaken;
    }

    public void setAllBooksTaken(List<Book> allBooksTaken) {
        this.allBooksTaken = allBooksTaken;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", currentBooksTaken=" + currentBooksTaken +
                ", allBooksTaken=" + allBooksTaken +
                '}';
    }
}
