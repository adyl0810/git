package lesson.lesson45;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Employees {
    List<Employee> employees = new ArrayList<>();
    Gson gson = new Gson();

    public Employees() {
        readFromJson();
    }

    public void readFromJson() {
        Type employeesType = new TypeToken<List<Employee>>() {
        }.getType();
        try (BufferedReader reader = new BufferedReader(new FileReader("employees.json"))) {
            String jsonTasks = reader.readLine();
            employees = gson.fromJson(jsonTasks, employeesType);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void printEmp() {
        for (int i = 0; i < employees.size(); i++) {
            System.out.println(employees.get(i));
        }
    }
}
