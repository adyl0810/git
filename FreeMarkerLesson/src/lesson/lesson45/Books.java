package lesson.lesson45;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Books {
    List<Book> books = new ArrayList<>();
    Gson gson = new Gson();

    public Books() {
        readFromJson();
    }

    public void readFromJson() {
        Type booksType = new TypeToken<List<Book>>() {
        }.getType();
        try (BufferedReader reader = new BufferedReader(new FileReader("books.json"))) {
            String jsonTasks = reader.readLine();
            books = gson.fromJson(jsonTasks, booksType);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public List<Book> getBooks() {
        return books;
    }
}
